<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Digital Play ))</title>
    <script
            src="https://code.jquery.com/jquery-3.5.1.min.js"
            integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0="
            crossorigin="anonymous"></script>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
          integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css"
          integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
    <style>
        .container {width: 960px;}
        .container input {margin: 10px;}
    </style>

    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"
            integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
</head>
<body>
<div class="container">
    <form action="" method="post">
        <input type="text" class="form-control" placeholder="Введите элементы через запятую"
               name="elements" value="<?=isset($_POST['elements'])?$_POST['elements']:'1,2,3,4,5,6,7,8,9,10';?>">
        <input type="text" name="columns_count" value="<?=isset($_POST['columns_count'])?$_POST['columns_count']:'4';?>">
        <input type="submit" class="btn btn-warning">
    </form>
    <pre>
<?
if(isset($_POST['elements']) && isset($_POST['columns_count'])) {
	$list = $_POST['elements'];
	$list = explode(',', $list);
	$n    = intval($_POST['columns_count']);
	print_r(splitList($list, $n));
}

/**
 * @param array$list
 * @param mixed $n
 *
 * @return array
 */
function splitList($list, $n)
{
	$elements_count = count($list);
	$rows_count = $elements_count % $n > 0? intval( $elements_count / $n) + 1: $elements_count / $n;
	$delta = $elements_count % $n > 0?1:0;
	$full_array_count = $n * $rows_count;
	$fake_elements_count = $full_array_count - $elements_count;
	$elements0 = $list;
	for ($fake_position = $full_array_count-1; $fake_position >= $elements_count-($rows_count);
		$fake_position = ($fake_position - ($rows_count))) {
		if($delta) {
			$list[$fake_position] = 'fake';
		}
		for ( $index = $fake_position + 1; $index < $full_array_count; $index ++ ) {
			$list[ $index ] = isset($elements0[ $index - 1 ])?$elements0[ $index - 1 ]:'fake';
		}
	}

	$out = [];
	for($row = 0; $row <= $rows_count-1; $row++) {
		for($column = 0; $column <= $n - 1; $column++) {
			$out[$row][$column] = isset($list[ $row + $column * ($rows_count)])?$list[ $row + $column * $rows_count]:'fake';
		}
	}

	$out2 = [];
	for($column = 0; $column <= $n - 1; $column++) {
		for($row = 0; $row <= $rows_count - 1; $row++) {
			$out2[$column][$row] = $out[$row][$column];
		}
	}
	return $out2;
}

?>
    </pre>
</div>
</body>
</html>