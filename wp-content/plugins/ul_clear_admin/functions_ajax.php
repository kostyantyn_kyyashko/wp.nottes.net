<?
function change_status()
{
    global $wpdb;
    global $current_user;
    $user_email = $current_user->user_email;
    $handler = $_POST['handler'];
    $sql = <<<sql
SELECT `ul`.`status` FROM `ul_clear_admin` `ul` WHERE `ul`.`handler` = '{$handler}' AND `ul`.`user_email` = '{$user_email}'
sql;
    $status = $wpdb->get_var($sql);
    if ($status == 'true') {
        $status = 'false';
    }
    else {
        $status = 'true';
    }
    $sql2 = <<<sql
UPDATE ul_clear_admin SET status = '{$status}' WHERE handler = '{$handler}' AND `user_email` = '{$user_email}'
sql;
    $wpdb->query($sql2);
    die('ok');
}

function change_order()
{
    global $wpdb;
    global $current_user;
    $user_email = $current_user->user_email;
    $order = $_POST['order_value'];
    $handler = $_POST['handler'];
    $sql = <<<sql
UPDATE ul_clear_admin SET `order` = {$order} WHERE handler = '{$handler}' AND `user_email` = '{$user_email}'
sql;
    $wpdb->query($sql);
    die('ok');
}

function change_icon_color()
{
    global $wpdb;
    global $current_user;
    $user_email = $current_user->user_email;
    $icon_color = $_POST['icon_color'];
    $handler = $_POST['handler'];
    $sql = <<<sql
UPDATE `ul_clear_admin` SET `icon_color` = '{$icon_color}' WHERE `handler` = '{$handler}' AND `user_email` = '{$user_email}'
sql;
    $wpdb->query($sql);
    echo 1;
    die();
}

function save_icon()
{
    global $wpdb;
    global $current_user;
    $user_email = $current_user->user_email;
    $handler = $_POST['handler'];
    $icon_css = str_replace('dashicons ', '', $_POST['icon_css']);
    $sql = <<<sql
UPDATE `ul_clear_admin`
SET `icon_css` = '{$icon_css}'
WHERE 
`user_email` = '{$user_email}'
AND 
`handler` = '{$handler}'
sql;
    $wpdb->query($sql);
    echo 1;
    die();
}

function ul_get_option($option_name)
{
    global $wpdb;
    wp_cookie_constants();
    require_once ABSPATH . WPINC . '/pluggable.php';
    $current_user = wp_get_current_user();
    $user_email = $current_user->user_email;
    $sql = <<<sql
SELECT option_value FROM ul_options WHERE option_name = '{$option_name}' AND user_email = '{$user_email}'
sql;
    return $wpdb->get_var($sql);
}

function ul_set_option($option_name, $option_value)
{
    global $wpdb;
    global $current_user;
    $user_email = $current_user->user_email;
        $sql = <<<sql
REPLACE INTO `ul_options`
(`option_name`, `option_value`, `user_email`)
VALUES 
('{$option_name}', '{$option_value}', '{$user_email}')
sql;
        $wpdb->query($sql);
}

function hide_admin_bar()
{
    if (isset($_POST['hide_admin_bar'])) {
        $current_hide_admin_bar = ul_get_option('hide_admin_bar');
        ul_set_option('hide_admin_bar', !$current_hide_admin_bar);
    }
    echo 1;
    die();
}

function hide_admin_bar_front()
{
    if (isset($_POST['hide_admin_bar_front'])) {
        $current_hide_admin_bar_front = ul_get_option('hide_admin_bar_front');
        ul_set_option('hide_admin_bar_front', !$current_hide_admin_bar_front);}
    echo 1;
    die();
}

function hide_admin_main_page()
{
    if (isset($_POST['hide_admin_main_page'])) {
        $current_hide_main_admin_page = ul_get_option('hide_admin_main_page');
        ul_set_option('hide_admin_main_page', !$current_hide_main_admin_page);
    }
    echo 1;
    die();
}

function change_selected_item_color()
{
    ul_set_option('selected_item_color', $_POST['selected_item_color']);
    echo 1;
    die();
}

global $ext;
function file_uploader()
{
    global $ext;
    $ext = false;
    $detected_type = exif_imagetype($_FILES['logo']['tmp_name']);
    switch ($detected_type) {
        case 1:
            $ext = 'gif';
            break;
        case 2: $ext = 'jpeg';
            break;
        case 3: $ext = 'png';
            break;
        default: echo 'error';
    }
    $plugin_path = plugin_dir_path(__FILE__);
    $shell_remove_command = "sudo rm -rf " .  $plugin_path ."img";
    @shell_exec($shell_remove_command);
    mkdir(plugin_dir_path(__FILE__) . 'img');
    $upload_file = plugin_dir_path(__FILE__) . "img/logo.$ext";
    $upload_file_path = plugin_dir_path(__FILE__) . "img/";
    move_uploaded_file($_FILES['logo']['tmp_name'], $upload_file);

    switch ($detected_type) {
        case 1:
            $image_data = imagecreatefromgif($upload_file);
            $logo64 = imagescale($image_data, 100 );
            imagegif($logo64, $upload_file_path . 'logo64.' . $ext);
            break;
        case 2:
            $image_data = imagecreatefromjpeg($upload_file);
            $logo64 = imagescale($image_data, 100 );
            imagejpeg($logo64, $upload_file_path . 'logo64.' . $ext);
            break;
        case 3:
            $image_data = imagecreatefrompng($upload_file);
            $logo64 = imagescale($image_data, 100 );
            imagepng($logo64, $upload_file_path . 'logo64.' . $ext);
            break;
    }

    switch ($detected_type) {
        case 1:
            $image_data = imagecreatefromgif($upload_file);
            $logo32 = imagescale($image_data, 32, 32);
            imagegif($logo32, $upload_file_path . 'logo32.' . $ext);
            break;
        case 2:
            $image_data = imagecreatefromjpeg($upload_file);
            $logo32 = imagescale($image_data, 32, 32);
            imagejpeg($logo32, $upload_file_path . 'logo32.' . $ext);
            break;
        case 3:
            $image_data = imagecreatefrompng($upload_file);
            $logo32 = imagescale($image_data, 32, 32);
            imagepng($logo32, $upload_file_path . 'logo32.' . $ext);
            break;
    }
    file_put_contents($upload_file_path . 'ext.txt', $ext);
    die();
}
