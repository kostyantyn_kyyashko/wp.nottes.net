<?php
/*
* Created by Kostyantyn Kyyashko
* k.konstantin.n@gmail.com
*/
function plugin_activate()
{
    create_clear_db_table();
    fill_data_to_db();
    setup_init_values();
    dashboard_page_init();
}

function plugin_deactivate()
{
    drop_db_table();
}
