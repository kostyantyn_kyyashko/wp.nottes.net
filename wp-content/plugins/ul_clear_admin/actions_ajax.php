<?php
add_action( 'wp_ajax_change_status', 'change_status' );
add_action( 'wp_ajax_change_order', 'change_order' );
add_action('wp_ajax_change_icon_color', 'change_icon_color');
add_action('wp_ajax_save_icon', 'save_icon');
add_action('wp_ajax_hide_admin_bar', 'hide_admin_bar');
add_action('wp_ajax_hide_admin_bar_front', 'hide_admin_bar_front');
add_action('wp_ajax_change_selected_item_color', 'change_selected_item_color');
add_action('wp_ajax_change_icon_color', 'change_icon_color');
add_action('wp_ajax_file_uploader', 'file_uploader');
add_action('wp_ajax_hide_admin_main_page', 'hide_admin_main_page');




