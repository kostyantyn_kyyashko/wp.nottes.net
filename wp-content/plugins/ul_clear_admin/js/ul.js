/*
* Created by Kostyantyn Kyyashko
* k.konstantin.n@gmail.com
*/
function change_status() {
    $('.status').click(function () {
        var handler = $(this).parent().find('.handler').text();
        $.ajax({
            url: ajaxurl,
            type: 'post',
            data: {
                action: 'change_status',
                handler: handler
            },
            success: function (resp) {
                if (resp === 'ok') {
                    alert('Change status success');
                    window.location.reload()
                }
            }
        })
    })
}

function change_order() {
    $('.order').change(function () {
        var order_value = $(this).val();
        var handler = $(this).parent().parent().find('.handler').text();
        $.ajax({
            url: ajaxurl,
            type: 'post',
            data: {
                action: 'change_order',
                order_value: order_value,
                handler: handler
            },
            success: function () {
                window.location.reload();
            }
        })
    })
}

function change_color()
{
    $('#icon_color').change(function () {
        var color = $(this).val();
        var td_class = $(this).parent().parent().find('.td_icon_class').text();
        var handler = $(this).parent().find('.handler').text();
        $.ajax({
            url: ajaxurl,
            type: 'post',
            data: {
                action: 'change_icon_color',
                color: color,
                handler: $.cookie('ul_handler')
            },
            success: function (resp) {
                if (parseInt(resp) === 1) {
                    alert('Color change Successfully')
                    window.location.reload();
                }
                else {
                    alert('Error change icon color')
                }
            }
        })
    })
}

function modal_icons()
{
    $('.td-icon').click(function () {
        var handler= $(this).parent().find('.handler').text();
        $.cookie('ul_handler', handler, { expires: 7, path: '/' });
        $('#exampleModalLong').modal({})
    })
}

function icon_choose() {
    $('[data=icon]').unbind();
    $('[data=icon]').click(function () {
        var icon_css = $(this).attr('class');
        if (confirm('Change Current Icon?')) {
            $.ajax({
                url: ajaxurl,
                type: 'post',
                data: {
                    action: 'save_icon',
                    icon_css: icon_css,
                    handler: $.cookie('ul_handler')
                },
                success: function (resp) {
                    if (parseInt(resp) === 1) {
                        alert('Icon change successfully!');
                        window.location.reload()
                    }
                    else {
                        alert('Error! Ico not change')
                    }
                }
            })
        }
    })
}

/*
function hide_admin_main_page_setup() {
    $('#hide_main_admin_page').click(function () {
        $.ajax({
            url: ajaxurl,
            type: 'post',
            data: {
                action: 'hide_admin_main_page',
                hide_admin_bar: 1
            },
            success: function (resp) {
                if (parseInt(resp) === 1) {
                    window.location.reload()
                }
                else {
                    alert ('Error update admin bar')
                }
            }
        })
    })
}
*/

function hide_admin_bar() {
    $('#hide_show_admin').click(function () {
        $.ajax({
            url: ajaxurl,
            type: 'post',
            data: {
                action: 'hide_admin_bar',
                hide_admin_bar: 1
            },
            success: function (resp) {
                    if (resp) {
                    window.location.reload();
                }
                else {
                    alert ('Error update admin bar')
                }
            }
        })
    })
}

function hide_admin_bar_front() {
    $('#hide_show_admin_front').click(function () {
        $.ajax({
            url: ajaxurl,
            type: 'post',
            data: {
                action: 'hide_admin_bar_front',
                hide_admin_bar_front: 1
            },
            success: function (resp) {
                if (parseInt(resp) === 1)
                {
                    window.open('/', '_blank');
                    window.location.reload();
                }
                else {
                    alert ('Error update admin bar')
                }
            }
        })
    })
}

function hide_admin_main_page()
{
    $('#hide_main_admin_page').click(function () {
        $.ajax({
            url: ajaxurl,
            type: 'post',
            data: {
                action: 'hide_admin_main_page',
                hide_admin_main_page: 1
            },
            success: function (resp) {
                if(parseInt(resp) === 1) {
                    window.location.reload()
                }
                else {
                    alert("Dashboars change error")
                }
            }
        })
    })
}

function choose_selected_item_color() {
    $('#choose_selected_item_color').change(function () {
        var selected_item_color = $(this).val();
        $(this).css({'background-color': selected_item_color});
            $.ajax({
                url: ajaxurl,
                type: 'post',
                data: {
                    action: 'change_selected_item_color',
                    selected_item_color: selected_item_color
                },
                success: function (resp) {
                        window.location.reload();
                }
            })
    })
}

function change_icon_color() {
    $('#icon_color').change(function () {
        var icon_color = $(this).val();
        var handler = $.cookie('ul_handler') ;
        $(this).css({'background-color': icon_color});
            $.ajax({
                url: ajaxurl,
                type: 'post',
                data: {
                    handler: handler,
                    action: 'change_icon_color',
                    icon_color: icon_color
                },
                success: function (resp) {
                        //window.location.reload();
                }
            })
    })
}

function file_uploader()
{
    $('[name=logo]').change(function () {
        var form = document.getElementById("file_uploader");
        $.ajax({
            url: ajaxurl,
            data: new FormData(form),
            type:"post",
            contentType:false,
            processData:false,
            cache:false,
            success:function(data){
                $('#logo_table').html(data);
                window.location.reload();
            }
        });
    })
}


$(document).ready(function () {
    change_status();
    change_order();
    change_color();
    modal_icons();
    icon_choose();
    hide_admin_bar();
    hide_admin_bar_front();
    choose_selected_item_color();
    change_icon_color();
    file_uploader();
    hide_admin_main_page();
});