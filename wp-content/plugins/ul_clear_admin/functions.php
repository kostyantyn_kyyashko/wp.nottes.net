<?php

function menu_items_control_table()
{
    global $wpdb;
    unset($menu);
    $table_header = <<<html
    <div class="table-responsive-sm" id="control_admin_menu_items">
    <h5>&#9660; Control Admin Menu Items</h5> 
    <table class="table table-striped table-hover table-responsive-sm table-bordered" id="admin_menu_items">
        <thead>
        <tr>
            <th data-tooltip 
                title="The menu items are arranged in the order defined by these numbers, in ascending order" 
                style="width: 1%">
                Order
            </th>
            <th data-tooltip 
                title="Name of dashboard menu. Not can be edited"
                style="width: 3%;">
                Menu Item
            </th>
            <th style="width: 3%;">User can</th>
            <th style="width: 3%;" title="relative URL or callable funcion">Handler<span style="color: blue; font-size: 130%">*</span> </th>
            <th style="width: 3%;">Icon View</th>
            <th style="width: 1%">Status</th>
        </tr>
        </thead>
        <tbody>
html;
    $sql2 = <<<sql
SELECT * from `ul_clear_admin` 
sql;

    $out = json_decode(json_encode($wpdb->get_results($sql2)), 1);
    $table = '';
    $table_rows = '';
    usort($out, function($a, $b) {
        if ($a['order'] > $b['order']) {
            return 1;
        }
        return -1;
   });
    foreach ($out as $key=>$item):
        if(strstr($item['handler'], 'separator')) continue;
        ob_start();
        ?>
        <tr>
            <td><input class="order" value="<?=$item['order']?>" style="width: 30px;"></td>
            <td><b><?=$item['menu_title']?></b></td>
            <td><?=$item['role']?></td>
            <? $handler = strstr($item['handler'], '.php')?$item['handler']:"admin.php?page={$item['handler']}" ?>
            <td class="handler"><a href="/wp-admin/<?=$handler?>"><?=$item['handler']?></a></td>
            <td class="align-middle td-icon" style="text-align: center;
                background-color: #23282D;">
                <div class="dashicons <?=$item['icon_css']?>"></div>
            </td>
            <td class="align-middle status" style="text-align: center;"><? if($item['status']=='true'): ?><span class="dashicons dashicons-admin-post"></span> <?endif;?></td>
        </tr>
        <?
        $table_rows .= ob_get_contents();
        ob_end_clean();
    endforeach;
    $table .= $table_header .  $table_rows . '</tbody></table></div>';
    $table = "<div style = font-size:80%>{$table}</div>";
    echo $table . '<hr class="separator">';
}

function buttons_section()
{
    $button_text_hide_in_admin = !ul_get_option('hide_admin_bar')?'Now Hides. Click to Show':'Now Shows. Click to Hide';
    $button_text_hide_in_front = !ul_get_option('hide_admin_bar_front')?'Now Hides. Click to Show':'Now Shows. Click to Hide';
    $button_text_hide_admin_main_page = !ul_get_option('hide_admin_main_page')?'Shows Dashboard':'Show your file';
    $button_class_admin_bar = ($button_text_hide_in_admin == 'Now Shows. Click to Hide')?'btn-info':'btn-warning';
    $button_class_admin_bar_front = ($button_text_hide_in_front == 'Now Shows. Click to Hide')?'btn-info':'btn-warning';
    $button_class_admin_main_page = ($button_text_hide_admin_main_page == 'Shows Dashboard')?'btn-info':'btn-warning';
    $select = colors_select();
    $table = <<<html
    <div class="table-responsive-sm" id="control_admin_menu_items">
    <h5>&#9660; Control Top ToolBar</h5> 
    <table class="table table-striped table-hover table-responsive-sm table-bordered" style="width: 600px;">
        <tbody>
        <tr>
            <td style="text-align: left;">Top admin <b>toolbar</b> in the <b>dashboard</b></td>
            <td style="text-align: right;">Top admin <b>toolbar</b> in the <b>site</b></td>
        </tr>
        <tr>
            <td style="width: 300px;">
                <button class="btn {$button_class_admin_bar} btn-sm" id="hide_show_admin"><b>{$button_text_hide_in_admin}</b></button>
            </td>
            <td style="width: 300px; text-align: right;">
                <button class="btn {$button_class_admin_bar_front} btn-sm" id="hide_show_admin_front"><b>{$button_text_hide_in_front}</b></button>
            </td>
        </tr>
        </tbody>
    </table>
     
    <table class="table table-striped table-hover table-responsive-sm table-bordered" style="width: 600px;">
        <tbody>
        <tr>
            <td style="text-align: left;">Choose Selected Item Color</td>
            <td style="text-align: right;">Show <b>DashBoard<b/> or <b>Your File<b/></td>
        </tr>
        <tr>
            <td style="width: 300px;" data-tooltip title="Click to color and choose fine color">
                {$select}
            </td>
            <td style="width: 300px; text-align: right;">
                <button class="btn {$button_class_admin_main_page} btn-sm" id="hide_main_admin_page"><b>{$button_text_hide_admin_main_page}</b></button>
            </td>
        </tr>
        </tbody>
    </table>
</div>
html;
    echo $table;
}

function colors_select()
{
    ob_start();
    ?>
        <select class="form-control" id="choose_selected_item_color">
            <option selected disabled value="#" style="visibility: hidden"></option>
            <? $icon_colors = all_colors();
            foreach ($icon_colors as $color_name => $hex_value):?>
                <option  value="<?=$color_name?>"
                        style="background-color: <?=$hex_value?>;"><?=$color_name?></option>
            <? endforeach;?>
        </select>
<?
    $select = ob_get_contents();
    ob_end_clean();
    return $select;
}



function uploader_in_admin_panel()
{
    ?>
    <h5 class="color_select">&#9660; Choose and change your logo</h5>
    <div>
        <form id="file_uploader" method="post" >
            <input type="text" name="action" value="file_uploader" hidden="hidden" />
            <input type="file" name="logo" />
        </form>
    </div>
    <div id="logo_table">
        <table class="table table-striped" style="width: auto;">
            <tbody>
            <tr>
                <td>Logo in the login page</td>
                <td>Favicon</td>
            </tr>
            <tr>
                <?
                $ext = '';
                if(file_exists(plugin_dir_path(__FILE__) . 'img/logo64.gif')) {
                    $ext = 'gif';
                }
                if(file_exists(plugin_dir_path(__FILE__) . 'img/logo64.jpeg'))  {
                    $ext = 'jpeg';
                }
                if(file_exists(plugin_dir_path(__FILE__) . 'img/logo64.png')) {
                    $ext = 'png';
                }
                if ($ext) {
                    ?>
                    <td><img src="<?= plugin_dir_url(__FILE__) ?>img/logo64.<?= $ext ?>"></td>
                    <td><img src="<?= plugin_dir_url(__FILE__) ?>img/logo32.<?= $ext ?>"></td>
                    <?
                }
                else {
                    ?>
                    <td><img src="<?= plugin_dir_url(__FILE__) ?>img_original/logowp.png"></td>
                    <td><img src="<?= plugin_dir_url(__FILE__) ?>img_original/logowp32.png"></td>
                    <?
                }
                ?>
            </tr>
            </tbody>
        </table>
    </div>
    <?
}

function all_colors()
{
    $out = [];
    $out['black'] = '#000000';
    $out['white'] = '#FFFFFF';
    $out['red'] = '#FF0000';
    $out['lime'] = '#00FF00';
    $out['blue'] = '#0000FF';
    $out['yellow'] = '#FFFF00';
    $out['orange'] = '#FFC107';
    $out['cyan'] = '#00FFFF';
    $out['magenta'] = '#FF00FF';
    $out['silver'] = '#C0C0C0';
    $out['gray'] = '#808080';
    $out['maroon'] = '#800000';
    $out['olive'] = '#808000';
    $out['green'] = '#008000';
    $out['purple'] = '#800080';
    $out['teal'] = '#008080';
    $out['navy'] = '#000080';

    return $out;
}

function create_clear_db_table()
{
    global $wpdb;
    $sql = <<<sql
CREATE TABLE IF NOT EXISTS `ul_clear_admin` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `menu_title` varchar(255) DEFAULT NULL,
  `role` varchar(255) DEFAULT NULL,
  `handler` varchar(255) DEFAULT NULL,
  `page_title` varchar(255) DEFAULT NULL,
  `menu_css` varchar(255) DEFAULT NULL,
  `css_class` text DEFAULT NULL,
  `icon_css` varchar(255) DEFAULT NULL,
  `order` int(11) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `color` varchar (20) DEFAULT NULL,
  `icon_color` varchar (20) DEFAULT NULL,
  `user_email` varchar (100) DEFAULT NULL, 
  PRIMARY KEY (`id`),
  UNIQUE INDEX email_unique (`user_email`, `handler`)
)
ENGINE = INNODB,
CHARACTER SET utf8,
COLLATE utf8_general_ci;
sql;
    $wpdb->query($sql);

    $sql2 = <<<sql
CREATE TABLE IF NOT EXISTS ul_options (
  id int(11) NOT NULL AUTO_INCREMENT,
  `user_email` varchar(255) DEFAULT NULL,
  `option_name` varchar(255) DEFAULT NULL,
  `option_value` varchar(255) DEFAULT NULL,
  PRIMARY KEY (id),
  UNIQUE INDEX email_unique (`user_email`, `option_name`)
)
ENGINE = INNODB,
CHARACTER SET utf8,
COLLATE utf8_general_ci;
sql;
    $wpdb->query($sql2);
}

function add_admin_menu_page_to_menu_and_db()
{
    add_menu_page('Clear Admin', 'Clear Admin', 'read',
        'clear_admin','assembler', 'dashicons-list-view', 10);
    global $wpdb;
    global $current_user;
    $user_email = $current_user->user_email;
    $menu_css = <<<css
menu-top menu-icon-post open-if-no-js menu-top-first
css;
    $css_class = "menu-appearance2";
    $icon_css = "dashicons-list-view";
    $sql = <<<sql
INSERT IGNORE INTO `ul_clear_admin`
(`menu_title`, `role`, `handler`, `page_title`, `menu_css`, `css_class`, `icon_css`, `order`, `status`, `color`, `icon_color`, `user_email`)
VALUES 
('Clear Admin','read','clear_admin','Clear Admin','{$menu_css}', '{$css_class}', '{$icon_css}', 0, 'true', 'red', 'red', '{$user_email}')        
sql;
    $wpdb->query($sql);
}


function fill_data_to_db()
{
    global $menu;
    global $wpdb;
    global $current_user;
    $user_email = $current_user->user_email;
    foreach ($menu as $key=>$out) {
        $out[5] = isset($out[5])?$out[5]:null;
        $out[6] = isset($out[6])?$out[6]:null;
        $sql = <<<sql
INSERT IGNORE INTO `ul_clear_admin`
(`menu_title`, `role`, `handler`, `page_title`, `menu_css`, `css_class`, `icon_css`, `order`, `status`, `color`, `icon_color`, `user_email`)
VALUES 
('{$out[0]}','{$out[1]}','{$out[2]}','{$out[3]}','{$out[4]}','{$out[5]}','{$out[6]}',{$key},'true', 'null', 'null', '{$user_email}')
sql;
        $wpdb->query($sql);
    }
    $data = ob_get_contents();
}

function drop_db_table()
{
    global $wpdb;
    $sql = <<<sql
DROP TABLE IF EXISTS `ul_clear_admin`;
sql;
    $wpdb->query($sql);
    $sql2 = <<<sql
DROP TABLE IF EXISTS `ul_options`;
sql;
    $wpdb->query($sql2);

}

function ext()
{
    if (file_exists(plugin_dir_path(__FILE__) . 'img/ext.txt')) {
        return file_get_contents(plugin_dir_url(__FILE__) . 'img/ext.txt');
    }
    return null;
}

global $icons_style;
function add_scripts_to_admin() {
    global $icons_style;
    wp_enqueue_style('bootstrap_css', plugin_dir_url(__FILE__) . 'css/bootstrap.css');
    wp_enqueue_style('style1', plugin_dir_url(__FILE__) . 'css/clear.css');
    ?><link rel="stylesheet" href="<?=plugin_dir_url(__FILE__) . 'css/tooltip.css'?>"> <?
    if ($ext = ext()):
    ?><link rel="shortcut icon" href="<?=plugin_dir_url(__FILE__)?>img/logo32.<?=$ext;?>"/><?
        endif;?><?
    ?><script src="<?=plugin_dir_url(__FILE__)?>js/jquery.js'"></script><?
    ?><script src="<?=plugin_dir_url(__FILE__)?>js/bootstrap.js'"></script><?
    ?><script src="<?=plugin_dir_url(__FILE__)?>js/popper.js'"></script><?
    ?><script src="<?=plugin_dir_url(__FILE__)?>js/jquery.cookie.js'"></script><?
    ?><script src="<?=plugin_dir_url(__FILE__)?>js/tooltip.js'"></script><?
    ?><script src="<?=plugin_dir_url(__FILE__)?>js/ul.js'"></script><?
    if(in_array('clear_admin', $_GET)) {
        wp_enqueue_style('custom_for_table', plugin_dir_url(__FILE__) . 'css/custom_for_table.css');
    }
    ?>
    <style><?=$icons_style?></style>
    <div class="modal fade" id="exampleModalLong" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
    <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-body">
                <? require plugin_dir_path(__FILE__) . 'icons.php'; echo $admin_menu_icons?>
              </div>
            </div>
        </div>
    </div>
<?
}

function add_scripts_to_front()
{
    if ($ext = ext()) {
        wp_enqueue_style('style2', plugin_dir_url(__FILE__) . 'css/clear_front.css');
        ?>
        <link rel="shortcut icon" href="<?=$ext;?>"/>
        <?
    }
    return;
}

function hide_show_admin_bar()
{
    if (!ul_get_option('hide_admin_bar')) {
        ?>
        <style>
            #wpadminbar {display:none !important;}
            #wpwrap {margin-top: -40px !important;}
        </style>
        <?
    }
}

function hide_show_admin_bar_front()
{
    if (!ul_get_option('hide_admin_bar_front')) {
        ?>
        <style>
            #wpadminbar {display:none !important;}
            #site-header {margin-top: -40px !important;}
        </style>
        <?
    }
}



function menu_items_after_clear()
{
    global $menu;
    global $wpdb;
    global $icons_style;
    global $current_user;
    $user_email = $current_user->user_email;
    $sql2 = <<<sql
SELECT * FROM `ul_clear_admin` WHERE `status` = 'true' AND `user_email` = '{$user_email}' 
sql;

    $out = json_decode(json_encode($wpdb->get_results($sql2)), 1);

    usort($out, function($a, $b) {
        if ($a['order'] > $b['order']) {
            return 1;
        }
        return -1;
    });
    foreach ($out as $o) {
        $sub_out[$o['order']] = $o;
    }
    $d = [];
    $icons_style = '';
    foreach ($sub_out as $order => $inner_array){
        unset($inner_array['id']);
        if ($inner_array['icon_color']) {
            $icons_style .= <<<css
.{$inner_array['icon_css']}, div.{$inner_array['icon_css']}:before { color: {$inner_array['icon_color']} !important; }
css;
        }
        $d[$order] = array_values($inner_array);
    }
    $menu = $d;
}

function clear_wp_dash()
{
    $current_url = get_current_screen()->id;
    if (ul_get_option('hide_admin_main_page') && $current_url == 'dashboard') {
        ?>
        <style>
            .wrap h1, #dashboard-widgets, #screen-meta, #contextual-help-link, #show-settings-link {display: none !important;}
            #wpcontent: {margin-top 10px !important;}
            #wpcontent: {pagging-top 10px !important;}
        </style>
        <?
        include_once plugin_dir_path(__FILE__) . 'dashboard/index.php';
    }
}
function menu_items_color()
{
    $background_color = ul_get_option('selected_item_color');
    $css = "";
    if ($background_color) {
        $css = "
        <style>
            li.current div, .wp-has-current-submenu, #choose_selected_item_color {
                background-color: {$background_color} !important;
            }
        </style>
";
    }
    echo $css;
}

function ul_logo()
{
    if ($ext = ext()) {
            ?>
            <style>
                body.login h1 a {
                    background: url(<?=plugin_dir_url(__FILE__) . 'img/logo64.' . $ext;?>) no-repeat;
                }
            </style>
            <?
        }
}

function custom_login_logo_url()
{
    return get_site_url();
}

function custom_login_logo_url_title() {
    return get_bloginfo('name');
}

function add_favicon() {
    if ($ext = ext()) {
        ob_start();
        ?>
        <link rel="shortcut icon" href="<?=plugin_dir_url(__FILE__)?>img/logo32.<?=$ext;?>"/>
        <?
        $style = ob_get_contents();
        ob_end_clean();
        echo  $style;
    }
    return null;
}

function setup_init_values()
{
    ul_set_option('hide_admin_bar', 1);
    ul_set_option('hide_admin_bar_front', 1);
    ul_set_option('index_hide', 1);
    ul_set_option('selected_item_color', 'blue');
}

function dashboard_page_init() {}