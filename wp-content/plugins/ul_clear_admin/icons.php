<h5 style="color: #7254DF; margin-bottom: 20px;">&#9660; Choose Icon Color
<select style="width: 150px !important;" class="form-control" id="icon_color">
                <option selected disabled value="#" style="visibility: hidden"></option>
                <?      $icon_colors = all_colors();
                foreach ($icon_colors as $color_name => $hex_value):?>
                        <option class="form-control" value="<?=$color_name?>" style="background-color: <?=$hex_value?>"><?=$color_name?></option>
                <? endforeach;?>
        </select>
        </h5>
        <?
global $admin_menu_icons;
$admin_menu_icons = '
<div id="icons_set">
    <h5 style="color: #7254DF;">&#9660; Click to Icon for choose new menu Icon </h5>
    <div data="icon" alt="f333" class="dashicons dashicons-menu"></div>
    <div data="icon" alt="f228" class="dashicons dashicons-menu-alt"></div>
    <div data="icon" alt="f329" class="dashicons dashicons-menu-alt2"></div>
    <div data="icon" alt="f349" class="dashicons dashicons-menu-alt3"></div>
    <div data="icon" alt="f319" class="dashicons dashicons-admin-site"></div>
    <div data="icon" alt="f11d" class="dashicons dashicons-admin-site-alt"></div>
    <div data="icon" alt="f11e" class="dashicons dashicons-admin-site-alt2"></div>
    <div data="icon" alt="f11f" class="dashicons dashicons-admin-site-alt3"></div>
    <div data="icon" alt="f226" class="dashicons dashicons-dashboard"></div>
    <div data="icon" alt="f109" class="dashicons dashicons-admin-post"></div>
    <div data="icon" alt="f104" class="dashicons dashicons-admin-media"></div>
    <div data="icon" alt="f103" class="dashicons dashicons-admin-links"></div>
    <div data="icon" alt="f105" class="dashicons dashicons-admin-page"></div>
    <div data="icon" alt="f101" class="dashicons dashicons-admin-comments"></div>
    <div data="icon" alt="f100" class="dashicons dashicons-admin-appearance"></div>
    <div data="icon" alt="f106" class="dashicons dashicons-admin-plugins"></div>
    <div data="icon" alt="f485" class="dashicons dashicons-plugins-checked"></div>
    <div data="icon" alt="f110" class="dashicons dashicons-admin-users"></div>
    <div data="icon" alt="f107" class="dashicons dashicons-admin-tools"></div>
    <div data="icon" alt="f108" class="dashicons dashicons-admin-settings"></div>
    <div data="icon" alt="f112" class="dashicons dashicons-admin-network"></div>
    <div data="icon" alt="f102" class="dashicons dashicons-admin-home"></div>
    <div data="icon" alt="f111" class="dashicons dashicons-admin-generic"></div>
    <div data="icon" alt="f148" class="dashicons dashicons-admin-collapse"></div>
    <div data="icon" alt="f536" class="dashicons dashicons-filter"></div>
    <div data="icon" alt="f540" class="dashicons dashicons-admin-customizer"></div>
    <div data="icon" alt="f541" class="dashicons dashicons-admin-multisite"></div>
    <hr>

    <!-- welcome screen -->
    <div data="icon" alt="f119" class="dashicons dashicons-welcome-write-blog"></div>
    <!--<div data="icon" alt="f119" class="dashicons dashicons-welcome-edit-page"></div> Duplicate -->
    <div data="icon" alt="f133" class="dashicons dashicons-welcome-add-page"></div>
    <div data="icon" alt="f115" class="dashicons dashicons-welcome-view-site"></div>
    <div data="icon" alt="f116" class="dashicons dashicons-welcome-widgets-menus"></div>
    <div data="icon" alt="f117" class="dashicons dashicons-welcome-comments"></div>
    <div data="icon" alt="f118" class="dashicons dashicons-welcome-learn-more"></div>

    <hr>

    <!-- post formats -->
    <!--<div data="icon" alt="f109" class="dashicons dashicons-format-standard"></div> Duplicate -->
    <div data="icon" alt="f123" class="dashicons dashicons-format-aside"></div>
    <div data="icon" alt="f128" class="dashicons dashicons-format-image"></div>
    <div data="icon" alt="f161" class="dashicons dashicons-format-gallery"></div>
    <div data="icon" alt="f126" class="dashicons dashicons-format-video"></div>
    <div data="icon" alt="f130" class="dashicons dashicons-format-status"></div>
    <div data="icon" alt="f122" class="dashicons dashicons-format-quote"></div>
    <!--<div data="icon" alt="f103" class="dashicons dashicons-format-links"></div> Duplicate -->
    <div data="icon" alt="f125" class="dashicons dashicons-format-chat"></div>
    <div data="icon" alt="f127" class="dashicons dashicons-format-audio"></div>
    <div data="icon" alt="f306" class="dashicons dashicons-camera"></div>
    <div data="icon" alt="f129" class="dashicons dashicons-camera-alt"></div>
    <div data="icon" alt="f232" class="dashicons dashicons-images-alt"></div>
    <div data="icon" alt="f233" class="dashicons dashicons-images-alt2"></div>
    <div data="icon" alt="f234" class="dashicons dashicons-video-alt"></div>
    <div data="icon" alt="f235" class="dashicons dashicons-video-alt2"></div>
    <div data="icon" alt="f236" class="dashicons dashicons-video-alt3"></div>

    <hr>

    <!-- media -->
    <div data="icon" alt="f501" class="dashicons dashicons-media-archive"></div>
    <div data="icon" alt="f500" class="dashicons dashicons-media-audio"></div>
    <div data="icon" alt="f499" class="dashicons dashicons-media-code"></div>
    <div data="icon" alt="f498" class="dashicons dashicons-media-default"></div>
    <div data="icon" alt="f497" class="dashicons dashicons-media-document"></div>
    <div data="icon" alt="f496" class="dashicons dashicons-media-interactive"></div>
    <div data="icon" alt="f495" class="dashicons dashicons-media-spreadsheet"></div>
    <div data="icon" alt="f491" class="dashicons dashicons-media-text"></div>
    <div data="icon" alt="f490" class="dashicons dashicons-media-video"></div>
    <div data="icon" alt="f492" class="dashicons dashicons-playlist-audio"></div>
    <div data="icon" alt="f493" class="dashicons dashicons-playlist-video"></div>
    <div data="icon" alt="f522" class="dashicons dashicons-controls-play"></div>
    <div data="icon" alt="f523" class="dashicons dashicons-controls-pause"></div>
    <div data="icon" alt="f519" class="dashicons dashicons-controls-forward"></div>
    <div data="icon" alt="f517" class="dashicons dashicons-controls-skipforward"></div>
    <div data="icon" alt="f518" class="dashicons dashicons-controls-back"></div>
    <div data="icon" alt="f516" class="dashicons dashicons-controls-skipback"></div>
    <div data="icon" alt="f515" class="dashicons dashicons-controls-repeat"></div>
    <div data="icon" alt="f521" class="dashicons dashicons-controls-volumeon"></div>
    <div data="icon" alt="f520" class="dashicons dashicons-controls-volumeoff"></div>

    <hr>

    <!-- image editing -->
    <div data="icon" alt="f165" class="dashicons dashicons-image-crop"></div>
    <div data="icon" alt="f531" class="dashicons dashicons-image-rotate"></div>
    <div data="icon" alt="f166" class="dashicons dashicons-image-rotate-left"></div>
    <div data="icon" alt="f167" class="dashicons dashicons-image-rotate-right"></div>
    <div data="icon" alt="f168" class="dashicons dashicons-image-flip-vertical"></div>
    <div data="icon" alt="f169" class="dashicons dashicons-image-flip-horizontal"></div>
    <div data="icon" alt="f533" class="dashicons dashicons-image-filter"></div>
    <div data="icon" alt="f171" class="dashicons dashicons-undo"></div>
    <div data="icon" alt="f172" class="dashicons dashicons-redo"></div>

    <hr>

    <!-- databases -->
    <div data="icon" alt="f170" class="dashicons dashicons-database-add"></div>
    <div data="icon" alt="f17e" class="dashicons dashicons-database"></div>
    <div data="icon" alt="f17a" class="dashicons dashicons-database-export"></div>
    <div data="icon" alt="f17b" class="dashicons dashicons-database-import"></div>
    <div data="icon" alt="f17c" class="dashicons dashicons-database-remove"></div>
    <div data="icon" alt="f17d" class="dashicons dashicons-database-view"></div>

    <hr>

    <!-- block editor -->
    <div data="icon" alt="f134" class="dashicons dashicons-align-full-width"></div>
    <div data="icon" alt="f10a" class="dashicons dashicons-align-pull-left"></div>
    <div data="icon" alt="f10b" class="dashicons dashicons-align-pull-right"></div>
    <div data="icon" alt="f11b" class="dashicons dashicons-align-wide"></div>
    <div data="icon" alt="f12b" class="dashicons dashicons-block-default"></div>
    <div data="icon" alt="f11a" class="dashicons dashicons-button"></div>
    <div data="icon" alt="f137" class="dashicons dashicons-cloud-saved"></div>
    <div data="icon" alt="f13b" class="dashicons dashicons-cloud-upload"></div>
    <div data="icon" alt="f13c" class="dashicons dashicons-columns"></div>
    <div data="icon" alt="f13d" class="dashicons dashicons-cover-image"></div>
    <div data="icon" alt="f11c" class="dashicons dashicons-ellipsis"></div>
    <div data="icon" alt="f13e" class="dashicons dashicons-embed-audio"></div>
    <div data="icon" alt="f13f" class="dashicons dashicons-embed-generic"></div>
    <div data="icon" alt="f144" class="dashicons dashicons-embed-photo"></div>
    <div data="icon" alt="f146" class="dashicons dashicons-embed-post"></div>
    <div data="icon" alt="f149" class="dashicons dashicons-embed-video"></div>
    <div data="icon" alt="f14a" class="dashicons dashicons-exit"></div>
    <div data="icon" alt="f10e" class="dashicons dashicons-heading"></div>
    <div data="icon" alt="f14b" class="dashicons dashicons-html"></div>
    <div data="icon" alt="f14c" class="dashicons dashicons-info-outline"></div>
    <div data="icon" alt="f10f" class="dashicons dashicons-insert"></div>
    <div data="icon" alt="f14d" class="dashicons dashicons-insert-after"></div>
    <div data="icon" alt="f14e" class="dashicons dashicons-insert-before"></div>
    <div data="icon" alt="f14f" class="dashicons dashicons-remove"></div>
    <div data="icon" alt="f15e" class="dashicons dashicons-saved"></div>
    <div data="icon" alt="f150" class="dashicons dashicons-shortcode"></div>
    <div data="icon" alt="f151" class="dashicons dashicons-table-col-after"></div>
    <div data="icon" alt="f152" class="dashicons dashicons-table-col-before"></div>
    <div data="icon" alt="f15a" class="dashicons dashicons-table-col-delete"></div>
    <div data="icon" alt="f15b" class="dashicons dashicons-table-row-after"></div>
    <div data="icon" alt="f15c" class="dashicons dashicons-table-row-before"></div>
    <div data="icon" alt="f15d" class="dashicons dashicons-table-row-delete"></div>

    <hr>

    <!-- tinymce -->
    <div data="icon" alt="f200" class="dashicons dashicons-editor-bold"></div>
    <div data="icon" alt="f201" class="dashicons dashicons-editor-italic"></div>
    <div data="icon" alt="f203" class="dashicons dashicons-editor-ul"></div>
    <div data="icon" alt="f204" class="dashicons dashicons-editor-ol"></div>
    <div data="icon" alt="f12c" class="dashicons dashicons-editor-ol-rtl"></div>
    <div data="icon" alt="f205" class="dashicons dashicons-editor-quote"></div>
    <div data="icon" alt="f206" class="dashicons dashicons-editor-alignleft"></div>
    <div data="icon" alt="f207" class="dashicons dashicons-editor-aligncenter"></div>
    <div data="icon" alt="f208" class="dashicons dashicons-editor-alignright"></div>
    <div data="icon" alt="f209" class="dashicons dashicons-editor-insertmore"></div>
    <div data="icon" alt="f210" class="dashicons dashicons-editor-spellcheck"></div>
    <!-- <div data="icon" alt="f211" class="dashicons dashicons-editor-distractionfree"></div> Duplicate -->
    <div data="icon" alt="f211" class="dashicons dashicons-editor-expand"></div>
    <div data="icon" alt="f506" class="dashicons dashicons-editor-contract"></div>
    <div data="icon" alt="f212" class="dashicons dashicons-editor-kitchensink"></div>
    <div data="icon" alt="f213" class="dashicons dashicons-editor-underline"></div>
    <div data="icon" alt="f214" class="dashicons dashicons-editor-justify"></div>
    <div data="icon" alt="f215" class="dashicons dashicons-editor-textcolor"></div>
    <div data="icon" alt="f216" class="dashicons dashicons-editor-paste-word"></div>
    <div data="icon" alt="f217" class="dashicons dashicons-editor-paste-text"></div>
    <div data="icon" alt="f218" class="dashicons dashicons-editor-removeformatting"></div>
    <div data="icon" alt="f219" class="dashicons dashicons-editor-video"></div>
    <div data="icon" alt="f220" class="dashicons dashicons-editor-customchar"></div>
    <div data="icon" alt="f221" class="dashicons dashicons-editor-outdent"></div>
    <div data="icon" alt="f222" class="dashicons dashicons-editor-indent"></div>
    <div data="icon" alt="f223" class="dashicons dashicons-editor-help"></div>
    <div data="icon" alt="f224" class="dashicons dashicons-editor-strikethrough"></div>
    <div data="icon" alt="f225" class="dashicons dashicons-editor-unlink"></div>
    <div data="icon" alt="f320" class="dashicons dashicons-editor-rtl"></div>
    <div data="icon" alt="f10c" class="dashicons dashicons-editor-ltr"></div>
    <div data="icon" alt="f474" class="dashicons dashicons-editor-break"></div>
    <div data="icon" alt="f475" class="dashicons dashicons-editor-code"></div>
    <!-- <div data="icon" alt="f494" class="dashicons dashicons-editor-code-duplicate"></div> Duplicate -->
    <div data="icon" alt="f476" class="dashicons dashicons-editor-paragraph"></div>
    <div data="icon" alt="f535" class="dashicons dashicons-editor-table"></div>

    <hr>

    <!-- posts -->
    <div data="icon" alt="f135" class="dashicons dashicons-align-left"></div>
    <div data="icon" alt="f136" class="dashicons dashicons-align-right"></div>
    <div data="icon" alt="f134" class="dashicons dashicons-align-center"></div>
    <div data="icon" alt="f138" class="dashicons dashicons-align-none"></div>
    <div data="icon" alt="f160" class="dashicons dashicons-lock"></div>
    <!-- <div data="icon" alt="f315" class="dashicons dashicons-lock-duplicate"></div> Duplicate -->
    <div data="icon" alt="f528" class="dashicons dashicons-unlock"></div>
    <div data="icon" alt="f145" class="dashicons dashicons-calendar"></div>
    <div data="icon" alt="f508" class="dashicons dashicons-calendar-alt"></div>
    <div data="icon" alt="f177" class="dashicons dashicons-visibility"></div>
    <div data="icon" alt="f530" class="dashicons dashicons-hidden"></div>
    <div data="icon" alt="f173" class="dashicons dashicons-post-status"></div>
    <div data="icon" alt="f464" class="dashicons dashicons-edit"></div>
    <div data="icon" alt="f182" class="dashicons dashicons-trash"></div>
    <div data="icon" alt="f537" class="dashicons dashicons-sticky"></div>

    <hr>

    <!-- sorting -->
    <div data="icon" alt="f504" class="dashicons dashicons-external"></div>
    <div data="icon" alt="f142" class="dashicons dashicons-arrow-up"></div>
    <!-- <div data="icon" alt="f143" class="dashicons dashicons-arrow-up-duplicate"></div> Duplicate -->
    <div data="icon" alt="f140" class="dashicons dashicons-arrow-down"></div>
    <div data="icon" alt="f139" class="dashicons dashicons-arrow-right"></div>
    <div data="icon" alt="f141" class="dashicons dashicons-arrow-left"></div>
    <div data="icon" alt="f342" class="dashicons dashicons-arrow-up-alt"></div>
    <div data="icon" alt="f346" class="dashicons dashicons-arrow-down-alt"></div>
    <div data="icon" alt="f344" class="dashicons dashicons-arrow-right-alt"></div>
    <div data="icon" alt="f340" class="dashicons dashicons-arrow-left-alt"></div>
    <div data="icon" alt="f343" class="dashicons dashicons-arrow-up-alt2"></div>
    <div data="icon" alt="f347" class="dashicons dashicons-arrow-down-alt2"></div>
    <div data="icon" alt="f345" class="dashicons dashicons-arrow-right-alt2"></div>
    <div data="icon" alt="f341" class="dashicons dashicons-arrow-left-alt2"></div>
    <div data="icon" alt="f156" class="dashicons dashicons-sort"></div>
    <div data="icon" alt="f229" class="dashicons dashicons-leftright"></div>
    <div data="icon" alt="f503" class="dashicons dashicons-randomize"></div>
    <div data="icon" alt="f163" class="dashicons dashicons-list-view"></div>
    <div data="icon" alt="f164" class="dashicons dashicons-excerpt-view"></div>
    <div data="icon" alt="f509" class="dashicons dashicons-grid-view"></div>
    <div data="icon" alt="f545" class="dashicons dashicons-move"></div>

    <hr>

    <!-- social -->
    <div data="icon" alt="f237" class="dashicons dashicons-share"></div>
    <div data="icon" alt="f240" class="dashicons dashicons-share-alt"></div>
    <div data="icon" alt="f242" class="dashicons dashicons-share-alt2"></div>
    <div data="icon" alt="f303" class="dashicons dashicons-rss"></div>
    <div data="icon" alt="f465" class="dashicons dashicons-email"></div>
    <div data="icon" alt="f466" class="dashicons dashicons-email-alt"></div>
    <div data="icon" alt="f467" class="dashicons dashicons-email-alt2"></div>
    <div data="icon" alt="f325" class="dashicons dashicons-networking"></div>
    <div data="icon" alt="f162" class="dashicons dashicons-amazon"></div>
    <div data="icon" alt="f304" class="dashicons dashicons-facebook"></div>
    <div data="icon" alt="f305" class="dashicons dashicons-facebook-alt"></div>
    <div data="icon" alt="f18b" class="dashicons dashicons-google"></div>
    <!-- <div data="icon" alt="f462" class="dashicons dashicons-googleplus"></div> Defunct -->
    <div data="icon" alt="f12d" class="dashicons dashicons-instagram"></div>
    <div data="icon" alt="f18d" class="dashicons dashicons-linkedin"></div>
    <div data="icon" alt="f192" class="dashicons dashicons-pinterest"></div>
    <div data="icon" alt="f19c" class="dashicons dashicons-podio"></div>
    <div data="icon" alt="f195" class="dashicons dashicons-reddit"></div>
    <div data="icon" alt="f196" class="dashicons dashicons-spotify"></div>
    <div data="icon" alt="f199" class="dashicons dashicons-twitch"></div>
    <div data="icon" alt="f301" class="dashicons dashicons-twitter"></div>
    <div data="icon" alt="f302" class="dashicons dashicons-twitter-alt"></div>
    <div data="icon" alt="f19a" class="dashicons dashicons-whatsapp"></div>
    <div data="icon" alt="f19d" class="dashicons dashicons-xing"></div>
    <div data="icon" alt="f19b" class="dashicons dashicons-youtube"></div>

    <hr>

    <!-- WPorg specific icons: Jobs, Profiles, WordCamps -->
    <div data="icon" alt="f308" class="dashicons dashicons-hammer"></div>
    <div data="icon" alt="f309" class="dashicons dashicons-art"></div>
    <div data="icon" alt="f310" class="dashicons dashicons-migrate"></div>
    <div data="icon" alt="f311" class="dashicons dashicons-performance"></div>
    <div data="icon" alt="f483" class="dashicons dashicons-universal-access"></div>
    <div data="icon" alt="f507" class="dashicons dashicons-universal-access-alt"></div>
    <div data="icon" alt="f486" class="dashicons dashicons-tickets"></div>
    <div data="icon" alt="f484" class="dashicons dashicons-nametag"></div>
    <div data="icon" alt="f481" class="dashicons dashicons-clipboard"></div>
    <div data="icon" alt="f487" class="dashicons dashicons-heart"></div>
    <div data="icon" alt="f488" class="dashicons dashicons-megaphone"></div>
    <div data="icon" alt="f489" class="dashicons dashicons-schedule"></div>
    <div data="icon" alt="f10d" class="dashicons dashicons-tide"></div>
    <div data="icon" alt="f124" class="dashicons dashicons-rest-api"></div>
    <div data="icon" alt="f13a" class="dashicons dashicons-code-standards"></div>

    <hr>

    <!-- BuddyPress and bbPress specific icons -->
    <div data="icon" alt="f452" class="dashicons dashicons-buddicons-activity"></div>
    <div data="icon" alt="f477" class="dashicons dashicons-buddicons-bbpress-logo"></div>
    <div data="icon" alt="f448" class="dashicons dashicons-buddicons-buddypress-logo"></div>
    <div data="icon" alt="f453" class="dashicons dashicons-buddicons-community"></div>
    <div data="icon" alt="f449" class="dashicons dashicons-buddicons-forums"></div>
    <div data="icon" alt="f454" class="dashicons dashicons-buddicons-friends"></div>
    <div data="icon" alt="f456" class="dashicons dashicons-buddicons-groups"></div>
    <div data="icon" alt="f457" class="dashicons dashicons-buddicons-pm"></div>
    <div data="icon" alt="f451" class="dashicons dashicons-buddicons-replies"></div>
    <div data="icon" alt="f450" class="dashicons dashicons-buddicons-topics"></div>
    <div data="icon" alt="f455" class="dashicons dashicons-buddicons-tracking"></div>

    <hr>

    <!-- internal/products -->
    <div data="icon" alt="f120" class="dashicons dashicons-wordpress"></div>
    <div data="icon" alt="f324" class="dashicons dashicons-wordpress-alt"></div>
    <div data="icon" alt="f157" class="dashicons dashicons-pressthis"></div>
    <div data="icon" alt="f463" class="dashicons dashicons-update"></div>
    <div data="icon" alt="f113" class="dashicons dashicons-update-alt"></div>
    <div data="icon" alt="f180" class="dashicons dashicons-screenoptions"></div>
    <div data="icon" alt="f348" class="dashicons dashicons-info"></div>
    <div data="icon" alt="f174" class="dashicons dashicons-cart"></div>
    <div data="icon" alt="f175" class="dashicons dashicons-feedback"></div>
    <div data="icon" alt="f176" class="dashicons dashicons-cloud"></div>
    <div data="icon" alt="f326" class="dashicons dashicons-translation"></div>

    <hr>

    <!-- taxonomies -->
    <div data="icon" alt="f323" class="dashicons dashicons-tag"></div>
    <div data="icon" alt="f318" class="dashicons dashicons-category"></div>

    <hr>

    <!-- widgets -->
    <div data="icon" alt="f480" class="dashicons dashicons-archive"></div>
    <div data="icon" alt="f479" class="dashicons dashicons-tagcloud"></div>
    <div data="icon" alt="f478" class="dashicons dashicons-text"></div>

    <hr>

    <!-- alerts/notifications/flags -->
    <div data="icon" alt="f16d" class="dashicons dashicons-bell"></div>
    <div data="icon" alt="f147" class="dashicons dashicons-yes"></div>
    <div data="icon" alt="f12a" class="dashicons dashicons-yes-alt"></div>
    <div data="icon" alt="f158" class="dashicons dashicons-no"></div>
    <div data="icon" alt="f335" class="dashicons dashicons-no-alt"></div>
    <div data="icon" alt="f132" class="dashicons dashicons-plus"></div>
    <div data="icon" alt="f502" class="dashicons dashicons-plus-alt"></div>
    <div data="icon" alt="f543" class="dashicons dashicons-plus-alt2"></div>
    <div data="icon" alt="f460" class="dashicons dashicons-minus"></div>
    <div data="icon" alt="f153" class="dashicons dashicons-dismiss"></div>
    <div data="icon" alt="f159" class="dashicons dashicons-marker"></div>
    <div data="icon" alt="f155" class="dashicons dashicons-star-filled"></div>
    <div data="icon" alt="f459" class="dashicons dashicons-star-half"></div>
    <div data="icon" alt="f154" class="dashicons dashicons-star-empty"></div>
    <div data="icon" alt="f227" class="dashicons dashicons-flag"></div>
    <div data="icon" alt="f534" class="dashicons dashicons-warning"></div>

    <hr>

    <!-- misc/cpt -->
    <div data="icon" alt="f230" class="dashicons dashicons-location"></div>
    <div data="icon" alt="f231" class="dashicons dashicons-location-alt"></div>
    <div data="icon" alt="f178" class="dashicons dashicons-vault"></div>
    <div data="icon" alt="f332" class="dashicons dashicons-shield"></div>
    <div data="icon" alt="f334" class="dashicons dashicons-shield-alt"></div>
    <div data="icon" alt="f468" class="dashicons dashicons-sos"></div>
    <div data="icon" alt="f179" class="dashicons dashicons-search"></div>
    <div data="icon" alt="f181" class="dashicons dashicons-slides"></div>
    <div data="icon" alt="f121" class="dashicons dashicons-text-page"></div>
    <div data="icon" alt="f183" class="dashicons dashicons-analytics"></div>
    <div data="icon" alt="f184" class="dashicons dashicons-chart-pie"></div>
    <div data="icon" alt="f185" class="dashicons dashicons-chart-bar"></div>
    <div data="icon" alt="f238" class="dashicons dashicons-chart-line"></div>
    <div data="icon" alt="f239" class="dashicons dashicons-chart-area"></div>
    <div data="icon" alt="f307" class="dashicons dashicons-groups"></div>
    <div data="icon" alt="f338" class="dashicons dashicons-businessman"></div>
    <div data="icon" alt="f12f" class="dashicons dashicons-businesswoman"></div>
    <div data="icon" alt="f12e" class="dashicons dashicons-businessperson"></div>
    <div data="icon" alt="f336" class="dashicons dashicons-id"></div>
    <div data="icon" alt="f337" class="dashicons dashicons-id-alt"></div>
    <div data="icon" alt="f312" class="dashicons dashicons-products"></div>
    <div data="icon" alt="f313" class="dashicons dashicons-awards"></div>
    <div data="icon" alt="f314" class="dashicons dashicons-forms"></div>
    <div data="icon" alt="f473" class="dashicons dashicons-testimonial"></div>
    <div data="icon" alt="f322" class="dashicons dashicons-portfolio"></div>
    <div data="icon" alt="f330" class="dashicons dashicons-book"></div>
    <div data="icon" alt="f331" class="dashicons dashicons-book-alt"></div>
    <div data="icon" alt="f316" class="dashicons dashicons-download"></div>
    <div data="icon" alt="f317" class="dashicons dashicons-upload"></div>
    <div data="icon" alt="f321" class="dashicons dashicons-backup"></div>
    <div data="icon" alt="f469" class="dashicons dashicons-clock"></div>
    <div data="icon" alt="f339" class="dashicons dashicons-lightbulb"></div>
    <div data="icon" alt="f482" class="dashicons dashicons-microphone"></div>
    <div data="icon" alt="f472" class="dashicons dashicons-desktop"></div>
    <div data="icon" alt="f547" class="dashicons dashicons-laptop"></div>
    <div data="icon" alt="f471" class="dashicons dashicons-tablet"></div>
    <div data="icon" alt="f470" class="dashicons dashicons-smartphone"></div>
    <div data="icon" alt="f525" class="dashicons dashicons-phone"></div>
    <div data="icon" alt="f510" class="dashicons dashicons-index-card"></div>
    <div data="icon" alt="f511" class="dashicons dashicons-carrot"></div>
    <div data="icon" alt="f512" class="dashicons dashicons-building"></div>
    <div data="icon" alt="f513" class="dashicons dashicons-store"></div>
    <div data="icon" alt="f514" class="dashicons dashicons-album"></div>
    <div data="icon" alt="f527" class="dashicons dashicons-palmtree"></div>
    <div data="icon" alt="f524" class="dashicons dashicons-tickets-alt"></div>
    <div data="icon" alt="f526" class="dashicons dashicons-money"></div>
    <div data="icon" alt="f18e" class="dashicons dashicons-money-alt"></div>
    <div data="icon" alt="f328" class="dashicons dashicons-smiley"></div>
    <div data="icon" alt="f529" class="dashicons dashicons-thumbs-up"></div>
    <div data="icon" alt="f542" class="dashicons dashicons-thumbs-down"></div>
    <div data="icon" alt="f538" class="dashicons dashicons-layout"></div>
    <div data="icon" alt="f546" class="dashicons dashicons-paperclip"></div>
    <div data="icon" alt="f131" class="dashicons dashicons-color-picker"></div>
    <div data="icon" alt="f327" class="dashicons dashicons-edit-large"></div>
    <div data="icon" alt="f186" class="dashicons dashicons-edit-page"></div>
    <div data="icon" alt="f15f" class="dashicons dashicons-airplane"></div>
    <div data="icon" alt="f16a" class="dashicons dashicons-bank"></div>
    <div data="icon" alt="f16c" class="dashicons dashicons-beer"></div>
    <div data="icon" alt="f16e" class="dashicons dashicons-calculator"></div>
    <div data="icon" alt="f16b" class="dashicons dashicons-car"></div>
    <div data="icon" alt="f16f" class="dashicons dashicons-coffee"></div>
    <div data="icon" alt="f17f" class="dashicons dashicons-drumstick"></div>
    <div data="icon" alt="f187" class="dashicons dashicons-food"></div>
    <div data="icon" alt="f188" class="dashicons dashicons-fullscreen-alt"></div>
    <div data="icon" alt="f189" class="dashicons dashicons-fullscreen-exit-alt"></div>
    <div data="icon" alt="f18a" class="dashicons dashicons-games"></div>
    <div data="icon" alt="f18c" class="dashicons dashicons-hourglass"></div>
    <div data="icon" alt="f18f" class="dashicons dashicons-open-folder"></div>
    <div data="icon" alt="f190" class="dashicons dashicons-pdf"></div>
    <div data="icon" alt="f191" class="dashicons dashicons-pets"></div>
    <div data="icon" alt="f193" class="dashicons dashicons-printer"></div>
    <div data="icon" alt="f194" class="dashicons dashicons-privacy"></div>
    <div data="icon" alt="f198" class="dashicons dashicons-superhero"></div>
    <div data="icon" alt="f197" class="dashicons dashicons-superhero-alt"></div>
</div>
    ';?>
