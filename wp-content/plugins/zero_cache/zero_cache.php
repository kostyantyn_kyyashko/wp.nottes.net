<?php
/**
 * Plugin Name: Zero Cache
 */
function callback($buffer) {
    $uri = $_SERVER['REQUEST_URI'];
    $file = str_replace(['/', '.'. ','], '_', $uri);
    if (file_exists($file)) {
        return file_get_contents(plugin_dir_path(__FILE__) . $file);
    }
    else (file_put_contents(plugin_dir_path(__FILE__) . $file, $buffer))

    return $buffer;
}

function buffer_start() { ob_start("callback"); }

function buffer_end() { ob_end_flush(); }

add_action('wp_head', 'buffer_start');
add_action('wp_footer', 'buffer_end');